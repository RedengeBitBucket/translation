<?php
/**
 * Adapter
 * @package Redenge\Translation
 */
class Translate // implements IRedengeTranslator
{
	/** @var Redenge\Transaltion\Translator */
	protected static $translator;
	
	/** @var int */
	public static $idLanguage;

	/** @var bool */
	protected static $isLoaded = FALSE;


	private function __construct() {}


	private function __clone() {}


	private function __wakeup() {}


	static function init(ShopModel $shop, $idLanguage, $forceLoad = FALSE)
	{
		if (self::$isLoaded == FALSE || $forceLoad == TRUE || $idLanguage != self::$idLanguage) {

			$translates = [];

			$records = $shop->expression->getRecords('expression.code, expression_modul.code AS modul, expression_translate.translate',
				'JOIN expression_modul ON expression_modul.id = expression.id_expression_modul LEFT JOIN expression_translate ON expression_translate.id_expression=expression.id AND expression_translate.id_language = ' . intval($idLanguage));

			while ($row = mysqli_fetch_assoc($records)) {
				if (!array_key_exists($row['modul'], $translates)) {
					$translates[$row['modul']] = [];
				}

				$translates[$row['modul']][$row['code']] = $row['translate'];
			}

			mysqli_free_result($records);

			self::$idLanguage = $idLanguage;
			self::$translator = new \Redenge\Translation\Translator($translates);
			self::$isLoaded = TRUE;
		}
	}


	static function translate($modul, $code)
	{
		return self::$translator->translate($modul, $code);
	}


	static function translateAll($text)
	{
		return self::$translator->translateAll($text);
	}
}
