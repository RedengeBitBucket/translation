<?php

namespace Redenge\Translation;

/**
 * @author Vojtěch Lacina <MoraivaD1@gmail.com>
 */
interface IRedengeTranslator
{
	function translate($_modul, $_code, $variables = FALSE);

	function translateAll($_text);

	function translateLatteAll($_text);

	function translateLatteAllCb($matches);
}
