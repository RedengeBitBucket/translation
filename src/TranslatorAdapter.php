<?php

namespace Redenge\Translation;


use Nette\Object;
use Nette\Localization\ITranslator;
use Nette\Utils\Strings;


/**
 * @author Vojtěch Lacina <MoraivaD1@gmail.com>
 */
final class TranslatorAdapter extends Object implements ITranslator
{

	public function translate($message, $count = NULL)
	{
		$translation = explode('.', $message);
		if (!isset($translation[1])) {
			trigger_error("Your translation \"($translation[0])\" has wrong format.", E_USER_NOTICE);
			$translation[1] = '?';
		}

		$message = \Translate::translate($translation[0], $translation[1]);
		if ($count && $message[0] === '[' && Strings::substring($message, -1) === ']')
			$message = $this->addParams($message, $count);
		elseif ($count)
			$message = $this->replacePlaceholders($message, $count);

		return $message;
	}

	/**
	 * @param array $array
	 *
	 * @return bool
	 */
	private static function stringKeys(array $array)
	{
		if(count($array) <= 0)
		{
			return true;
		}

		return array_unique(array_map("is_string", array_keys($array))) === array(true);
	}

	/**
	 * @param string                $message
	 * @param array|string|int      $args
	 *
	 * @return string
	 */
	public static function replacePlaceholders($message, $args)
	{
		if (is_array($args) && !empty($args))
		{
			if (self::stringKeys($args))
				foreach ($args as $key => $value)
					$message = Strings::replace($message, "~(%{$key})~", $value);
			else
				foreach ($args as $value)
					$message = Strings::replace($message, '~(%[^ ,\.]+)~', $value, 1);

		}
		elseif (!is_array($args))
			$message = Strings::replace($message, '~(%[^ ,\.]+)~', (string) $args, 1);

		return $message;
	}

	/**
	 * @param string $message
	 * @param array|string|int $args
	 *
	 * @return string
	 */
	public function addParams($message, $args)
	{
		if (is_array($args) && !empty($args))
		{
			if (self::stringKeys($args))
				$message .= '(%' . implode(', %', array_keys($args)) . ')';
			else
				$message .= '(' . count($args) . ' unnamed params)';
		}
		elseif (!is_array($args))
			$message .= '(1 unnamed param)';

		return $message;
	}
}
