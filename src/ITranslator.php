<?php

namespace Redenge\Translation;

/**
 * @author Vojtěch Lacina <MoraivaD1@gmail.com>
 */
interface ITranslator
{
	function translate($message, $count = NULL);
}
