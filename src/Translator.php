<?php

namespace Redenge\Translation;

/**
 * @author Vojtěch Lacina <MoraivaD1@gmail.com>
 */
final class Translator implements IRedengeTranslator
{
	/** @var bool */
	private $isDevelopment;

	/** @var array */
	private $translates = [];


	private function __clone() {}


	private function __wakeup() {}


	public function __construct(array $translates, $isDevelopment = FALSE)
	{
		$this->translates = $translates;
		$this->isDevelopment = $isDevelopment;
	}


	public function translate($modul, $code, $variables = FALSE)
	{
		if ($this->isDevelopment) {
			$info = (object) [
				'module' => $modul,
				'code' => $code,
				'found' => isset($this->translates[$modul][$code]),
				'translation' => isset($this->translates[$modul][$code]) ? $this->translates[$modul][$code] : '',
			];
			Dumper::add($info, 'translations');
		}

		$text = isset($this->translates[$modul][$code]) ? $this->translates[$modul][$code] : FALSE;

		/**
		 * Fill all given variables in translation
		 */
		if ($variables && $text !== FALSE) {
			foreach ($variables as $key => $value) {
				$text = preg_replace('/%' . $key . '%/', $value, $text);
			}
		}

		return $text ? $text : "[$modul.$code]";
	}


	/**
	 * Find "{module:code}" pattern in text and translate it
	 * @info TemplatePower
	 *
	 * @return string Translated string
	 */
	public function translateAll($text)
	{
		return preg_replace_callback('/\{(\w+)\:(\w+)\}/', [$this, 'translateAllCb'], $text);
	}


	/**
	 * @info TemplatePower
	 *
	 * @return string Translated string
	 */
	private function translateAllCb($matches)
	{
		return $this->translate($matches[1], $matches[2]);
	}


	/**
	 * Find "{module:code}" pattern in text and translate it
	 * @info Latte support
	 *
	 * @return callback
	 */
	public function translateLatteAll($text)
	{
		return preg_replace_callback('/\{(\w+)\:(\w+)\}/', [$this, 'translateLatteAllCb'], $text);
	}


	/**
	 * @info Latte support
	 *
	 * @return string PHP snippet
	 */
	public function translateLatteAllCb($matches)
	{
		return "<?php echo Translate::translate('$matches[1]', '$matches[2]'); ?>";
	}
}
