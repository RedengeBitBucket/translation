# Redenge Translation

## Installation

1) Install via **composer**
```
#!bash
composer require redenge/transaltion
```
## Usage

Remove original `Translate.php` class.
> This package contains own `Translate` class, which is compatible with the original one.

### Replecements
*"Selected value is %value, nice :)"*
```
$translator->translate('module.translation', 5]);
# Returns "Selected value is 5, nice :)"
```
*"Selected value is %value. Key is %key"*
```
$translator->translate('module.translation', [5, 'foo']);
$translator->translate('module.translation', ['value' => 5, 'key' => 'foo']);
# Both returns "Selected value is 5. Key is foo"
```
### Missing translations
```
$translator->translate('module.translation'); # returns [module.translation]
$translator->translate('module.translation', 'foo'); # returns [module.translation](1 unnamed param)
$translator->translate('module.translation', ['foo', 'bar']); # returns [module.translation](2 unnamed params)
$translator->translate('module.translation', ['foo' => 5, 'bar' => 'baz']); # returns [module.translation](%foo, %bar)
```

## Requirements

- PHP >=5.6